var request = require('request');
var ParallelRequest = require('parallel-http-request');

// Run with command : node name.js file
// 2 endpoints being called in parallel upon different methods, executing infinite number of times

function intevalFunc() {
var request = new ParallelRequest(); 
request.add({
   url:'http://httpbin.org/post',
   method:'POST',
   headers: {'Content-Type' :'application/json'},
   body: {"name":"KaribskiPirati","salary":'7000',"age":'2000'}
})
.add({
  url:'https://jsonplaceholder.typicode.com/posts/2',
  method:'PUT',
  body: {
  title:	"kat kat kat"
  }
})
.send(function (response){
  console.log(response);
}); 
}
setInterval(intevalFunc, 50);

// one endpoint being called 5 times
function sendRequest(){
    request = {
        url:'http://dummy.restapiexample.com/api/v1/create',
        json: true,
        body: { 
            id: "123",
            name: 'SomeQAtestIDnew',
            salary: '4444',
            age: '111'
      }
    }
    require('request').post(request, (err,  response, body) => {
      console.log(response.body);
  });
}

concurrentRequests(5)
function concurrentRequests(limit){
  for (var i = 0; i < limit; i++) {
    sendRequest(i, function(res , body){
      var reqID = JSON.parse(body)
      var sCode = res.statusCode
      console.log( reqID + sCode)
    })
  }
}
 console.log("Running concurrent requests!")
  concurrentRequests()

